import React from 'react';
import './AboutUs.css';

const AboutUs = () => {
    return (
        <div className="About">
            <div className="about-pic">
                <div className="about-info">
                    <p className="about-p">Мы и есть дизайн-студия <span className="about-p_p">"Геометрия"</span></p>
                </div>
            </div>
            <h1>Наша команда</h1>
            <div className="team-grid">
                <div className="team_item active">
                    <div className="image_1"></div>
                    <h3>Попов Алексей</h3>
                    <p>Руководитель студии</p>
                </div>
                <div className="team_item active">
                    <div className="image_2"></div>
                    <h3>Анисимова Анна</h3>
                    <p>Дизайнер, 3D визуализатор</p>
                </div>
                <div className="team_item active">
                    <div className="image_3"></div>
                    <h3>Нестеров Игорь</h3>
                    <p>Маркетолог</p>
                </div>
            </div>
            <p className="why">Почему выбирают нас</p>
            <div className="blocks">
                <div className="boxes">
                    <div className="block-1 size-b">
                        <h4 className="icons">Работаем с 2008 года</h4>
                        <p><strong>
                            Команда Геометрия:
                            <br/>
                            * 25 дизайнеров;
                            <br/>
                            * 12 архитекторов;
                            <br/>
                            * 8 техников-архитекторов;
                            <br/>
                            * 6 комплектаторов;
                        </strong>
                        </p>
                    </div>
                    <div className="block-2 size-b">
                        <h4 className="icons">Командная работа</h4>
                        <p>13летний опыт показал, что лучший результат можно достигнуть только работая в команде.
                            Над вашим проектом будет трудится не только дизайнер или архитектор, а еще
                            архитектор-техник, проектный менеджер, дизайнер-комплектатор и декоратор;
                        </p>
                    </div>
                    <div className="block-3 size-b">
                        <h4 className="icons">Cобственный шоурум с материалами</h4>
                        <p>Для удобства наших клиентов мы построили собственный SHOW-ROOM, это экономит время наших
                            заказчиков и сотрудников, т. к. можно в одном месте рассмотреть предложения сразу нескольких
                            топовых производителей по каждой категории проекта.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AboutUs;