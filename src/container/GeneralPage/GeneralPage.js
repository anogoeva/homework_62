import React from 'react';
import './General.css';


const GeneralPage = () => {
    return (
        <div className="General">
            <div className="bottom-block">
                <div className="photo-zone">
                    <div className="container">
                        <div className="photo-text">
                            <h5>Дизайнерская студия <span>Геометрия</span> к вашим услугам</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div className="base_block">
                <div className="inner-block"><p><span>Геометрия</span> делает:</p>
                    <p>дизайн частных квартир</p>
                    <p>и проекты домов</p></div>
            </div>
        </div>
    );
};

export default GeneralPage;