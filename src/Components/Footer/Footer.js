import React from 'react';
import './Footer.css';

const Footer = () => {
    return (
        <div>
            <footer className="footer-container">
                <div className="footer">
                    <p>©2021 Все права защищены.</p>
                </div>
            </footer>
        </div>
    );
};

export default Footer;