import React from 'react';
import './Menu.css';

const Menu = () => {

    return (
        <div>
            <nav className="menu-container">
                <div className="menu">
                    <ul>
                        <li>
                            <a href="/">
                                Главная страница
                            </a>
                        </li>
                        <li>
                            <a href='/AboutUs'>
                                О нас
                            </a>
                        </li>
                        <li>
                            <a href="/Contact">
                                Контакты
                            </a>
                        </li>
                    </ul>
                    <ul>
                    </ul>
                </div>
            </nav>
        </div>
    );
};

export default Menu;