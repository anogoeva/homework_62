import {BrowserRouter, Route, Switch} from "react-router-dom";
import './App.css';
import GeneralPage from "./container/GeneralPage/GeneralPage";
import Menu from "./Components/Menu/Menu";
import Footer from "./Components/Footer/Footer";
import AboutUs from "./container/AboutUs/AboutUs";
import Contact from "./container/Contact/Contact";


const App = () => (
    <BrowserRouter>
        <Menu/>
        <div className="container">
            <Switch>
                <Route path="/" exact component={GeneralPage}/>
                <Route path="/AboutUs" component={AboutUs}/>
                <Route path="/Contact" component={Contact}/>
            </Switch>
        </div>
        <Footer/>
    </BrowserRouter>
);

export default App;
